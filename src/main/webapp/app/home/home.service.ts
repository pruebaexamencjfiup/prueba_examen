import { Observable } from 'rxjs';
import { HttpClient, HttpResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { IUsersApi } from './home.model';

export type EntityResponseType = HttpResponse<IUsersApi>;


@Injectable({ providedIn: 'root' })
export class HomeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/otherapi', 'microservice');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  getOtherUsersfromApi(): Observable<EntityResponseType> {
    return this.http.get<IUsersApi>(this.resourceUrl, {observe: 'response'});
  }

}
