export interface IUsersApi {
  id: string | null,
  name: string | null,
  gender: string | null,
  status: string | null;
  email: string | null;
}

export class UsersApi implements IUsersApi{

  constructor(
    public id: string,
    public name: string,
    public gender: string,
    public status: string,
    public email: string,
  ){}
}
