import * as dayjs from 'dayjs';

export interface IUsuario {
  id?: number;
  name?: string;
  lastName?: string;
  mlastName?: string | null;
  username?: string;
  birthday?: dayjs.Dayjs | null;
}

export class Usuario implements IUsuario {
  constructor(
    public id?: number,
    public name?: string,
    public lastName?: string,
    public mlastName?: string | null,
    public username?: string,
    public birthday?: dayjs.Dayjs | null
  ) {}
}

export function getUsuarioIdentifier(usuario: IUsuario): number | undefined {
  return usuario.id;
}
