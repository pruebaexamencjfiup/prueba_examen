package net.vaitech.examen.cucumber;

import io.cucumber.spring.CucumberContextConfiguration;
import net.vaitech.examen.PruebaExamenApp;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.web.WebAppConfiguration;

@CucumberContextConfiguration
@SpringBootTest(classes = PruebaExamenApp.class)
@WebAppConfiguration
public class CucumberTestContextConfiguration {}
