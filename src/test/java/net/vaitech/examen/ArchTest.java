package net.vaitech.examen;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {
        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("net.vaitech.examen");

        noClasses()
            .that()
            .resideInAnyPackage("net.vaitech.examen.service..")
            .or()
            .resideInAnyPackage("net.vaitech.examen.repository..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..net.vaitech.examen.web..")
            .because("Services and repositories should not depend on web layer")
            .check(importedClasses);
    }
}
